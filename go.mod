module denpendabot_go

go 1.21

require (
	github.com/me-cs/goRedisson v1.1.0
	github.com/redis/go-redis/v9 v9.4.0
)

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/satori/go.uuid v1.2.0 // indirect
)
