package main

import (
	"log"

	"github.com/me-cs/goRedisson"
	"github.com/redis/go-redis/v9"
)

func main() {
	// create redis client
	redisDB := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	defer redisDB.Close()

	g := goRedisson.NewGoRedisson(redisDB)
	lock := g.GetLock("example")
	err := lock.Lock()
	if err != nil {
		log.Print(err)
		return
	}

	//Your business code

	err = lock.Unlock()
	if err != nil {
		log.Print(err)
		return
	}

	return
}
